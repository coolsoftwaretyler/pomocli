# Pomocli

## Run locally 

```sh
deno run --allow-write --allow-env src/main.js
```
## Bundle 

```sh
deno bundle src/main.js dist/pomocli.js
```

## Compile as an executable 

```sh
deno compile --unstable --lite --output dist/bin/pomocli src/main.js
```

## Install from the URL

*We need write [permissions](https://deno.land/manual@v1.7.3/getting_started/permissions) to write to the log file, and env permissions to write it to the HOME directory*

```sh
deno install --allow-write --allow-env https://gitlab.com/coolsoftwaretyler/pomocli/-/raw/master/dist/pomocli.js
```

## Upgrade

If you're upgrading to a new version of Pomocli, you'll have to use the `--reload` flag for the installer:

```
deno install --allow-write --allow-env --reload -f https://gitlab.com/coolsoftwaretyler/pomocli/-/raw/master/dist/pomocli.js
```