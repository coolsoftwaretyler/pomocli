// Set the start time as the current time
const startTime = Date.now()

// Ask for the pomodor time and primary task
const timeLimit = await prompt("How much time (minutes)?")
const task = await prompt("What is your primary task this pomodoro?")

// Determine the end time based on the time input
const endTime = new Date(startTime + (1000 * 60 * timeLimit))

// Wait for the time limit to be up, print out the status periodically
const pomodoroInterval = await setInterval(pomodoro, 60000)

// Prompt function to write to stout and read from stdin
// https://www.danvega.dev/blog/2020/06/03/deno-stdin-stdout/
async function prompt(message = "") {
    const buf = new Uint8Array(1024);
    await Deno.stdout.write(new TextEncoder().encode(message + " "));
    const n = await Deno.stdin.read(buf);
    return new TextDecoder().decode(buf.subarray(0, n)).trim();
}

// If we have hit or surpassed the calculated endTime, wrap up.
// Otherwise, log the remaining time
function pomodoro() {
    const now = Date.now();
    if (endTime <= now) {
        wrapUp(pomodoroInterval)
    } else {
        const remainingTime = Math.floor(((endTime - now) / 1000) / 60)
        console.log(`Time remaining: ${remainingTime}`)
    }
}

// When the timer ends, notify in the system
async function wrapUp(pomodoroInterval) {
    clearInterval(pomodoroInterval)
    logResults();
}

// Write to the pomodoro log
async function logResults() {
    const status = await prompt("How did that go?")
    const entry = {
        startTime,
        timeLimit,
        task,
        endTime,
        status
    }
    await Deno.writeTextFile(`${Deno.env.get("HOME")}/.pomodoro.txt`, JSON.stringify(entry) + "\n", {append: true});
}