const startTime = Date.now();
const timeLimit = await prompt("How much time (minutes)?");
const task = await prompt("What is your primary task this pomodoro?");
const endTime = new Date(startTime + 1000 * 60 * timeLimit);
const pomodoroInterval = await setInterval(pomodoro, 60000);
async function prompt(message = "") {
    const buf = new Uint8Array(1024);
    await Deno.stdout.write(new TextEncoder().encode(message + " "));
    const n = await Deno.stdin.read(buf);
    return new TextDecoder().decode(buf.subarray(0, n)).trim();
}
function pomodoro() {
    const now = Date.now();
    if (endTime <= now) {
        wrapUp(pomodoroInterval);
    } else {
        const remainingTime = Math.floor((endTime - now) / 1000 / 60);
        console.log(`Time remaining: ${remainingTime}`);
    }
}
async function wrapUp(pomodoroInterval1) {
    clearInterval(pomodoroInterval1);
    logResults();
}
async function logResults() {
    const status = await prompt("How did that go?");
    const entry = {
        startTime,
        timeLimit,
        task,
        endTime,
        status
    };
    await Deno.writeTextFile(`${Deno.env.get("HOME")}/.pomodoro.txt`, JSON.stringify(entry) + "\n", {
        append: true
    });
}
